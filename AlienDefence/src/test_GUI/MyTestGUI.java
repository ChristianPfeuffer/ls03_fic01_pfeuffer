package test_GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyTestGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyTestGUI frame = new MyTestGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyTestGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 624);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(12, 13, 408, 48);
		contentPane.add(lblNewLabel);

		JLabel lblAufgabeHintergrundfarbe = new JLabel("Hintergrundfarbe \u00E4ndern");
		lblAufgabeHintergrundfarbe.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeHintergrundfarbe.setBounds(12, 62, 198, 16);
		contentPane.add(lblAufgabeHintergrundfarbe);

		JLabel lblTextFormatieren = new JLabel("Text formatieren");
		lblTextFormatieren.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTextFormatieren.setBounds(12, 152, 198, 16);
		contentPane.add(lblTextFormatieren);

		JLabel lblSchriftfarbendern = new JLabel("Schriftfarbe \u00E4ndern");
		lblSchriftfarbendern.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSchriftfarbendern.setBounds(12, 281, 198, 16);
		contentPane.add(lblSchriftfarbendern);

		JLabel lblSchriftgrendern = new JLabel("Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblSchriftgrendern.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSchriftgrendern.setBounds(12, 349, 198, 16);
		contentPane.add(lblSchriftgrendern);

		JLabel lblProgrammBeenden = new JLabel("Programm beenden");
		lblProgrammBeenden.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblProgrammBeenden.setBounds(12, 479, 198, 16);
		contentPane.add(lblProgrammBeenden);

		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text ");
		txtHierBitteText.setBounds(12, 216, 408, 22);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);

		JLabel lblTextausrichtung = new JLabel("Textausrichtung");
		lblTextausrichtung.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTextausrichtung.setBounds(12, 418, 198, 16);
		contentPane.add(lblTextausrichtung);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnExit.setBounds(12, 508, 408, 56);
		contentPane.add(btnExit);

		JButton btnBlau = new JButton("linksb\u00FCndig");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.LEADING);
			}
		});
		btnBlau.setBounds(12, 447, 116, 25);
		contentPane.add(btnBlau);

		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(157, 447, 116, 25);
		contentPane.add(btnZentriert);

		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(304, 447, 116, 25);
		contentPane.add(btnRechtsbndig);

		JButton btnFontRed = new JButton("Rot");
		btnFontRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.RED);
			}
		});
		btnFontRed.setBounds(12, 310, 116, 25);
		contentPane.add(btnFontRed);

		JButton btnFontBlue = new JButton("Blau");
		btnFontBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLUE);
			}
		});
		btnFontBlue.setBounds(157, 311, 116, 25);
		contentPane.add(btnFontBlue);

		JButton btnFontBlack = new JButton("Schwarz");
		btnFontBlack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLACK);
			}
		});
		btnFontBlack.setBounds(304, 311, 116, 25);
		contentPane.add(btnFontBlack);

		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		btnArial.setBounds(12, 181, 116, 25);
		contentPane.add(btnArial);

		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btnComicSansMs.setBounds(157, 181, 116, 25);
		contentPane.add(btnComicSansMs);

		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnCourierNew.setBounds(304, 181, 116, 25);
		contentPane.add(btnCourierNew);

		JButton btnBackgroundRot = new JButton("Rot");
		btnBackgroundRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.RED);
			}
		});
		btnBackgroundRot.setBounds(12, 91, 116, 25);
		contentPane.add(btnBackgroundRot);

		JButton btnBackgroundGreen = new JButton("Gruen");
		btnBackgroundGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.GREEN);
			}
		});
		btnBackgroundGreen.setBounds(157, 91, 116, 25);
		contentPane.add(btnBackgroundGreen);

		JButton btnBackgroundBlue = new JButton("Blau");
		btnBackgroundBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		btnBackgroundBlue.setBounds(304, 91, 116, 25);
		contentPane.add(btnBackgroundBlue);

		JButton btnBackgroundYellow = new JButton("Gelb");
		btnBackgroundYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		btnBackgroundYellow.setBounds(12, 129, 116, 25);
		contentPane.add(btnBackgroundYellow);

		JButton btnBackgroundStandardfarbe = new JButton("Standardfarbe");
		btnBackgroundStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(null);
			}
		});
		btnBackgroundStandardfarbe.setBounds(157, 129, 116, 25);
		contentPane.add(btnBackgroundStandardfarbe);

		JButton btnBackgroundFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnBackgroundFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color newColor = JColorChooser.showDialog(contentPane, "W�hle Farbe", contentPane.getBackground());
				if (newColor != null) {
					contentPane.setBackground(newColor);
				}
			}
		});
		btnBackgroundFarbeWhlen.setBounds(304, 129, 116, 25);
		contentPane.add(btnBackgroundFarbeWhlen);

		JButton btnCreateText = new JButton("Ins Label schreiben");
		btnCreateText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText(txtHierBitteText.getText());
			}
		});
		btnCreateText.setBounds(12, 251, 198, 25);
		contentPane.add(btnCreateText);

		JButton btnDeleteText = new JButton("Text im Label l\u00F6schen");
		btnDeleteText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText(null);
			}
		});
		btnDeleteText.setBounds(222, 251, 198, 25);
		contentPane.add(btnDeleteText);

		JButton btnBiggerFont = new JButton("+");
		btnBiggerFont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel.getFont().getSize();
				lblNewLabel.setFont((new Font(lblNewLabel.getFont().getFamily(), Font.PLAIN, groesse + 1)));
			}
		});
		btnBiggerFont.setBounds(12, 378, 198, 25);
		contentPane.add(btnBiggerFont);

		JButton btnSmallerFont = new JButton("-");
		btnSmallerFont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel.getFont().getSize();
				lblNewLabel.setFont((new Font(lblNewLabel.getFont().getFamily(), Font.PLAIN, groesse - 1)));
			}
		});
		btnSmallerFont.setBounds(222, 378, 198, 25);
		contentPane.add(btnSmallerFont);
	}
}