package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import controller.GameController;
import model.Level;
import model.User;
import model.persistenceDB.PersistenceDB;
import view.game.GameGUI;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class MainMenue extends JFrame {

	private static final long serialVersionUID = 1L;
	private AlienDefenceController alienDefenceController;
	private JPanel contentPane;
	private JTextField tfdLogin;
	private JPasswordField pfdPassword;
	private List<Level> arrLevel;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public MainMenue(AlienDefenceController alienDefenceController) {

		this.alienDefenceController = alienDefenceController;

		// Allgemeine JFrame-Einstellungen
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 475, 325);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Alles unter dem Bild, Tabellenlayout mit einer Spalte
		JPanel pnlButtons = new JPanel();
		pnlButtons.setBounds(5, 218, 447, 63);
		pnlButtons.setBackground(Color.BLACK);
		contentPane.add(pnlButtons);
		pnlButtons.setLayout(null);

		// Passwort
		JLabel lblPassword = new JLabel("Passwort:");
		lblPassword.setBounds(223, 0, 224, 25);
		lblPassword.setForeground(Color.orange);
		pnlButtons.add(lblPassword);

		pfdPassword = new JPasswordField();
		pfdPassword.setBounds(223, 26, 212, 25);
		pnlButtons.add(pfdPassword);

		//Levelnamen auslesen
		arrLevel = this.alienDefenceController.getLevelController().readAllLevels();
		String[] arrLevelNames = getLevelNames(arrLevel);
		
				// Nutzername
				JLabel lblLogin = new JLabel("Login:");
				lblLogin.setBounds(0, 0, 236, 25);
				pnlButtons.add(lblLogin);
				lblLogin.setForeground(Color.orange);
				
						tfdLogin = new JTextField();
						tfdLogin.setBounds(0, 26, 188, 25);
						pnlButtons.add(tfdLogin);
						tfdLogin.setColumns(10);

		//Logo		
		JPanel pnlLogo = new JPanel();
		pnlLogo.setBounds(21, 13, 212, 164);
		pnlLogo.setBackground(Color.BLACK);
		contentPane.add(pnlLogo);

		JLabel lblLogo = new JLabel("");
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("./pictures/logo.png").getImage().getScaledInstance(140, 140, Image.SCALE_DEFAULT));
		lblLogo.setIcon(imageIcon);
		GroupLayout gl_pnlLogo = new GroupLayout(pnlLogo);
		gl_pnlLogo.setHorizontalGroup(
			gl_pnlLogo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlLogo.createSequentialGroup()
					.addGap(50)
					.addComponent(lblLogo)
					.addContainerGap())
		);
		gl_pnlLogo.setVerticalGroup(
			gl_pnlLogo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlLogo.createSequentialGroup()
					.addGap(18)
					.addComponent(lblLogo)
					.addContainerGap(108, Short.MAX_VALUE))
		);
		pnlLogo.setLayout(gl_pnlLogo);
				
				//Spiel starten
				//Die Textfelder werden ausgewertet und das Passwort validiert, dann wird das Spiel gestartet
				JButton btnSpielen = new JButton("Spielen");
				btnSpielen.setBounds(270, 28, 168, 25);
				contentPane.add(btnSpielen);
				
				JButton btnTesten = new JButton("Testen");
				btnTesten.setBounds(270, 66, 168, 25);
				contentPane.add(btnTesten);
				
				JButton btnLeveleditor = new JButton("Leveleditor");
				btnLeveleditor.setBounds(270, 104, 168, 25);
				contentPane.add(btnLeveleditor);
				btnLeveleditor.setBackground(Color.ORANGE);
				
				JButton btnBeenden = new JButton("Exit");
				btnBeenden.setFont(new Font("Tahoma", Font.PLAIN, 13));
				btnBeenden.setBounds(372, 180, 66, 25);
				contentPane.add(btnBeenden);
				btnBeenden.setBackground(new Color(220, 20, 60));
				
				JButton btnHighscore = new JButton("Highscore");
				btnHighscore.setBounds(270, 142, 168, 25);
				contentPane.add(btnHighscore);
				
						// �berschrift
						JLabel lblHeadline = new JLabel("ALIEN DEFENCE");
						lblHeadline.setBounds(5, 180, 264, 31);
						contentPane.add(lblHeadline);
						lblHeadline.setHorizontalAlignment(SwingConstants.CENTER);
						lblHeadline.setForeground(new Color(124, 252, 0));
						lblHeadline.setFont(new Font("Stencil", Font.BOLD, 30));
				btnHighscore.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						new Highscore(alienDefenceController.getAttemptController(), arrLevel.get(1));
					}
				});
				btnBeenden.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				});
				btnLeveleditor.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						new LeveldesignWindow(alienDefenceController, null, "Leveleditor");
					}
				});
				btnTesten.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						// Erstellt Modell von aktuellen Nutzer
						User user = new User(1, "test", "pass");

						Thread t = new Thread("GameThread") {

							@Override
							public void run() {
								//Levelauswahl
								new LeveldesignWindow(alienDefenceController, user, "Testen");
							}
						};
						t.start();
					}
				});
				btnSpielen.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						btnSpielen_Clicked(alienDefenceController, arrLevel);
					}
				});
	}

	private String[] getLevelNames(List<Level> arrLevel) {
		String[] arrLevelNames = new String[arrLevel.size()];

		for (int i = 0; i < arrLevel.size(); i++) {
			arrLevelNames[i] = arrLevel.get(i).getName(); // Array aus Arraylist erstellt
		}

		return arrLevelNames;
	}
	
	public void btnSpielen_Clicked(AlienDefenceController alienDefenceController, List<Level> arrLevel) {
		// User aus Datenbank holen
		//TODO B�ser Versto� gegen MVC - hier muss sp�ter nochmal nachgebessert werden
		User user = new PersistenceDB().getUserPersistance().readUser(tfdLogin.getText());

		// Spielstarten, wenn Nutzer existiert und Passwort �bereinstimmt
		if (user != null && user.getPassword().equals(new String(pfdPassword.getPassword()))) {

			Thread t = new Thread("GameThread") {
				@Override
				public void run() {

					//GameController gameController = alienDefenceController.startGame(arrLevel.get(cboLevelChooser.getSelectedIndex()), user);
					//new GameGUI(gameController).start();
				}
			};
			t.start();
		} else {
			// Fehlermeldung - Zugangsdaten fehlerhaft
			JOptionPane.showMessageDialog(null, "Zugangsdaten nicht korrekt", "Fehler",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
